
from collections import OrderedDict
from copy import copy
import csv
from pathlib import Path

from ucp import utils


class CSVReader:
    def __init__(
            self,
            f,
            skiprows=0,
            headers=True,
            fieldnames=None,
            skip_blanks=True,
            parse_numbers=None,
            number_format=None,  # for output
            decimal='.',
            thousands='',
            parse_dates=None,
            date_format_in='%Y-%m-%d',
            date_format_out='%Y-%m-%d',
            lowercase_fields=None,
            strip_fields=None,
            **csvparams
    ):
        self.reader = csv.reader(f, **csvparams)
        self.skiprows = skiprows
        self.headers = headers
        self.fieldnames = fieldnames
        self.skip_blanks = skip_blanks
        self.parse_numbers = parse_numbers
        self.number_format = number_format
        self.decimal = decimal
        self.thousands = thousands
        self.parse_dates = parse_dates
        self.date_format_in = date_format_in
        self.date_format_out = date_format_out
        self.lowercase_fields = lowercase_fields
        self.strip_fields = strip_fields
        self.line_num = 0

    def __iter__(self):
        return self

    def _parse_fields(self, row):
        row_ = copy(row)
        for i, _ in enumerate(row):
            lowercase_field = (
                self.lowercase_fields is not None
                and (self.lowercase_fields is True or i in self.lowercase_fields)
            )
            strip_field = (
                self.strip_fields is not None
                and (self.strip_fields is True or i in self.strip_fields)
            )
            if lowercase_field:
                row_[i] = row_[i].lower()
            if strip_field:
                row_[i] = row_[i].strip()
            if self.parse_dates and i in self.parse_dates:
                row_[i] = utils.parse_date(
                    row_[i],
                    fmt=self.date_format_in,
                    outfmt=self.date_format_out
                )
            if self.parse_numbers and i in self.parse_numbers:
                row_[i] = utils.parse_number(
                    row_[i],
                    self.decimal,
                    self.thousands,
                    fmt=self.number_format
                )
        return row_

    def __next__(self):
        while self.reader.line_num < self.skiprows:
            next(self.reader)

        if self.line_num == 0 and self.headers:
            row = next(self.reader)
            self.line_num += 1
            if self.fieldnames is None:
                self.fieldnames = row

        row = next(self.reader)
        self.line_num += 1
        if self.fieldnames is None:
            self.fieldnames = list(range(len(row)))

        if self.skip_blanks:
            while row == []:
                row = next(self.reader)
        row = self._parse_fields(row)
        d = OrderedDict(zip(self.fieldnames, row))

        return d


def read_csv(filepath, encoding='utf-8', **kwargs):

    if not isinstance(filepath, Path):
        filepath = Path(filepath)

    with filepath.open(encoding=encoding, newline='') as f:
        reader = CSVReader(f, **kwargs)
        rows = list(reader)

    return rows
