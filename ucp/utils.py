"""Collection of utility functions"""
from datetime import datetime
from decimal import Decimal
import re


class dotdict(dict):
    """Enables dict.item syntax (instead of dict['item'])
    See http://stackoverflow.com/questions/224026
    """
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def parse_date(date, fmt=None, year=None, outfmt='%Y-%m-%d'):
    if not fmt:
        parsed_date = datetime.fromisoformat(date)
    else:
        parsed_date = datetime.strptime(date, fmt)
    if year:
        parsed_date = parsed_date.replace(year=year)
    return parsed_date.strftime(outfmt)


def parse_number(number, decimal='.', thousands='', ntype=float, fmt=None):

    if number == '':
        return None

    number = number.replace(' ', '').replace('+', '')
    if number.endswith('-'):
        number = '-' + number.replace('-', '')

    if decimal == thousands:
        msg = "`decimal` and `thousands` parameters cannot be the same"
        raise ValueError(msg)
    if thousands != '':
        number = number.replace(thousands, '')
    if decimal == ',':
        number = number.replace(',', '.')
    number = ntype(number)
    if fmt:
        number = format(number, fmt)

    return number


def map_value(hash_table, value) -> str:

    try:
        parsed_value = hash_table[value]
    except KeyError as e:
        print(f"Value {value} is not mapped")
        raise e

    return parsed_value


def parse_amount(amount, decimal='.', thousands='', type_=float):
    if decimal == thousands:
        msg = "`decimal` and `thousands` parameters cannot be the same"
        raise ValueError(msg)
    if thousands != '':
        amount = amount.replace(thousands, '')
    if decimal == ',':
        amount = amount.replace(',', '.')
    return "{amt:,.2f}".format(amt=type_(amount))


def search_pattern(pattern_str, search_str, group=0):
    pattern = re.compile(pattern_str)
    res = re.search(pattern, search_str)
    if res:
        return res.group(group).strip()
    return None


def parse_date_pattern(pattern_str, search_str, date_format):
    res = search_pattern(pattern_str, search_str)
    if res:
        return parse_date(res, date_format)
    return res


def record_to_str(record, fields=None):
    if not fields:
        fields = list(record.keys())
    if not isinstance(fields, list):
        fields = [fields]
    values = [str(record[field]) for field in fields]
    return ",".join(values)


def create_fee_records(records):
    fee_records = []
    for i, record in enumerate(records):
        if not record.get('fee'):
            continue
        amt = Decimal(record['amount'])
        fee = Decimal(record['fee'])
        new_amount = amt + fee if (amt < 0) else amt - fee
        record['amount'] = "{amt:,.2f}".format(amt=new_amount)
        fee_record = record.copy()
        fee_record['amount'] = "-{fee:,.2f}".format(fee=fee)
        fee_record['memo'] += ' fees'
        fee_records.append((i, fee_record))
    for i, fee_record in fee_records:
        records.insert(i, fee_record)
    return records
