
from copy import copy
import logging
from pathlib import Path
import re
from typing import Dict, List

from rule_engine import Rule

from ucp import utils
from ucp.reader import read_csv
from ucp.writer import write_csv


class CSVParser:

    file_name: str = None

    csvparams: Dict = None
    out_fields: List = None
    sort: Dict = None
    extract: Dict = None
    transform: Dict = None

    raw_data: List = None
    parsed_data: List = None

    def __init__(self, config: Dict, **kwargs):
        self.log = logging.getLogger(__name__)

        self.csvparams = config['csvparams']
        self.csvparams.update(kwargs)

        self.out_fields = config.get('out_fields', None)
        self.sort = config.get('sort', None)
        self.extract = config.get('extract', None)
        self.transform = config.get('transform', None)

    def get_output_path(self, outpath):
        if outpath is not None:
            outpath = Path(outpath)
        else:
            file_name = self.file_name + "_parsed"
            outpath = Path(".").resolve() / file_name

        if not outpath.suffix == ".csv":
            outpath = outpath.parent / (outpath.stem + ".csv")

        return outpath

    def read_file(self, filepath):

        self.file_name = filepath.name
        self.raw_data = read_csv(filepath, **self.csvparams)

    def write_file(self, outpath):

        outpath = self.get_output_path(outpath)
        write_csv(outpath, self.parsed_data, self.out_fields)

    @staticmethod
    def match_record(record, rule):

        search_fields = rule['search']
        search_str = utils.record_to_str(record, search_fields)
        pattern = rule['pattern']
        # TODO: set ignorecase as param
        match = re.search(pattern, search_str, re.IGNORECASE)

        return match

    def process_match(self, match, rule, kind):

        group = rule.get('group', 0)
        res = match.group(group)
        format_ = rule.get('format', None)
        outformat = rule.get('outformat', '%Y-%m-%d')
        year = rule.get('year', None)
        decimal = rule.get('decimal') or self.csvparams.get('decimal')
        thousands = rule.get('thousands') or self.csvparams.get('thousands')
        # type_ = rule.get('type', None)
        if kind == 'date':
            return utils.parse_date(res, fmt=format_, outfmt=outformat,
                                    year=year)
        if kind == 'number':
            return utils.parse_number(
                res, decimal=decimal, thousands=thousands, fmt=format_
            )

        return res

    def apply_rules(self, record, rules, kind):

        for rule in rules:
            match = self.match_record(record, rule)
            if match:
                return self.process_match(match, rule, kind)

        return None

    def extract_field(self, record, field):

        parts = self.extract[field]
        kind = parts.get('kind')
        fill_field = parts.get('fill_field')
        fill_value = parts.get('fill_value')
        rules = parts.get('rules')
        value = None
        if rules:
            value = self.apply_rules(record, rules, kind)

        if not value:
            if fill_field:
                value = record[fill_field]
            if fill_value:
                value = fill_value

        return value

    def transform_field(self, record, field):

        rules = self.transform[field]
        for parts in rules:
            rule = Rule(parts['rule'])
            if rule.matches(record):
                value = parts['action']
                return value.format(**record)

        return None

    def parse_record(self, record):

        parsed_record = copy(record)
        if self.extract:
            for field in self.extract:
                parsed_record[field] = self.extract_field(parsed_record, field)
        if self.transform:
            for field in self.transform:
                value = self.transform_field(parsed_record, field)
                if not value and field in parsed_record:
                    value = parsed_record[field]
                parsed_record[field] = value

        return parsed_record

    def parse_data(self):

        parsed_records = []
        for record in self.raw_data:
            parsed_record = self.parse_record(record)
            parsed_records.append(parsed_record)

        if self.sort:
            reverse = self.sort.get('reverse', False)
            parsed_records = sorted(
                parsed_records,
                key=lambda r: r[self.sort['by']],
                reverse=reverse
            )

        self.parsed_data = parsed_records

    def parse(self, filepath, outpath):

        try:
            self.read_file(filepath)
            self.parse_data()
            self.write_file(outpath)

        except Exception:
            logging.exception("Fatal exception ocurred: ")
            return False

        return True
