#!/usr/bin/env python
import argparse
import logging
from pathlib import Path
import sys

from ruamel.yaml import YAML

from ucp.parser import CSVParser


logging.basicConfig()


def load_config(confpath):
    yaml = YAML()
    with confpath.open() as f:
        config = yaml.load(f)
    return config


def get_args():
    parser = argparse.ArgumentParser(description="TX Processor script")
    parser.add_argument(
        'file', type=lambda p: Path(p).absolute(),
        help='The transaction file you want to parse'
    )
    parser.add_argument(
        '-c', '--config-path', type=lambda p: Path(p).absolute(),
        dest='config_path',
        help='The path to the config file'
    )
    parser.add_argument(
        '-o', '--output-path', type=lambda p: Path(p).absolute(),
        dest='outpath',
        help='The path to the output file'
    )
    args = parser.parse_args()
    return args


def parse_tx(filepath, outpath, config):
    parser = CSVParser(config)
    parser.parse(filepath, outpath)


def main():
    args = get_args()
    config = load_config(args.config_path)
    parse_tx(args.file, args.outpath, config)


if __name__ == "__main__":
    status = main()
    sys.exit(status)
