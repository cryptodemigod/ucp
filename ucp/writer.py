
import csv
from pathlib import Path


def write_csv(output_path, records, out_fields):

    if not isinstance(output_path, Path):
        output_path = Path(output_path)

    with output_path.open("w", newline='') as fp:
        csv_writer = csv.writer(fp, delimiter=',', quotechar='"',
                                quoting=csv.QUOTE_MINIMAL)
        for i, record in enumerate(records):
            if out_fields:
                record = {col: record[col] for col in out_fields}
            if i == 0:
                csv_writer.writerow(list(record.keys()))
            csv_writer.writerow(list(record.values()))
