#!/usr/bin/env python
"""
Release notes:
* Bump version in ucp/__init__.py
* Bump version in CHANGELOG.md
* Update docs where necessary
* Commit changes
* Tag version: git tag -a $version -m "version $version"
* Push changes (and tags)
"""
from pathlib import Path
from setuptools import find_packages, setup


__name__ = "ucp"
__version__ = None

init_file = Path(__file__).parent / __name__ / '__init__.py'
with init_file.open(encoding='utf-8') as f:
    for line in f:
        if not line.startswith('__version__'):
            continue
        __version__ = line.split('=')[1].strip(' "\'\n')
        break

with open("README.md", "r") as f:
    long_description = f.read()

with open("requirements.txt") as f:
    dependencies = [d.strip() for d in f.readlines() if not d.startswith('#')]


setup(
    name=__name__,
    version=__version__,
    packages=find_packages(),
    url='',
    license='',
    author='iuvbio',
    author_email='lyndonbjohn@gmail.com',
    description='Processes CSV to a standardised format',
    long_description=long_description,
    install_requires=dependencies,
    entry_points={
        'console_scripts': ['ucp=ucp.parsetx:main']
    }
)
