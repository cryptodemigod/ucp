Useless CSV Parser
=======

This module parses CSV files to other CSV files based on simple rules. It initially used `eval()` to evaluate parsing rules, hence the name *unsafe* parser. That was changed though, so it is now the *useless* CSV parser, because it still feels pretty clunky and suboptimal. However, I've been using it in several projects and it works quite well.
